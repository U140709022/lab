public class Circle {
	int radius;
	Point center;
	
	public Circle(int radius, Point center) {
		this.radius = radius;
		this.center = center;
		
	}
	
	public double area() {
		return (314 / 100) * (this.radius * this.radius);
	}
	
	public double perimeter() {
		return 2 * (this.radius * (314 / 100));
	}

	public boolean intersect(Circle c1){
        int top = this.radius + c1.radius;
        double dis = Math.sqrt(Math.pow((this.center.xCoord-c1.center.xCoord),2) +(Math.pow((this.center.yCoord-c1.center.yCoord),2)));
        if(dis<=top){
            return true;
        } 
        else{
        return false;    
        }
    }
}
