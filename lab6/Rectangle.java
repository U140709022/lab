public class Rectangle {
    int sideA, sideB;
    Point topLeft;
    public Rectangle(Point p, int a, int b){
        this.topLeft = p;
        this.sideA = a;
        this.sideB = b;
    }


    public int area(){
    
    return this.sideA * this.sideB;
    }

    public int perimeter(){
    
    return 2*(this.sideA+this.sideB);

    }
    public Point[] corners() {
        		
        Point bottomLeft = new Point(this.topLeft.xCoord, this.topLeft.yCoord - sideA);
		Point topRight = new Point(this.topLeft.xCoord + sideB, this.topLeft.yCoord );
		Point bottomRgiht = new Point(this.topLeft.xCoord + sideB, this.topLeft.yCoord - sideA);
		
		Point[] parr = new Point[4];
		parr[0] = this.topLeft;
		parr[1] = bottomLeft;
		parr[2] = topRight ;
		parr[3] = bottomRgiht;
						
		return parr;
	}
}
