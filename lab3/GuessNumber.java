import java.io.IOException;
import java.util.Random;
import java.util.Scanner;

public class GuessNumber {

    	
    
    public static void main(String[] args) throws IOException {	
		
        Scanner reader = new Scanner(System.in); //Creates an object to read user input
		Random rand = new Random(); //Creates an object from Random class
		int number =rand.nextInt(100); //generates a number between 0 and 99
		
		
		
		
		int guess;
        int c = 0;
		do{
            System.out.print("Type -1 to quit or guess another: ");
            guess = reader.nextInt();
	        c++;	
        if(guess == -1)
            System.out.println("Sorry, the number was " + number);
        
        else if (guess == number)
            System.out.println("Congratulations, you guessed at " + c +" attempts");
   		else if (guess > number){
            System.out.println("Sorry!");
		    System.out.println("Mine is less than your guess");
        }
        else {
            System.out.println("Sorry!");
		    System.out.println("Mine is greater than your guess");
        }
        
	    }while(guess != number && guess!= -1);
        
}
	
	
}

